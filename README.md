# Dotfiles Repository

This repository contains my personal dotfiles, including configurations for bspwm, polybar, kitty, zsh, picom, ranger and some neovim. Feel free to explore and use any parts of it that might be helpful for your own setup.

## Overview

- **bspwm**: A tiling window manager that organizes windows in a binary space partitioning manner.
- **polybar**: A fast and easy-to-use status bar that integrates with bspwm and other window managers.
- **kitty**: A modern, feature-rich terminal emulator.
- **zsh**: A powerful shell with additional features and customization.
- **picom**: A compositor for X11, providing window transparency and other visual effects.
- **ranger**: An amazing file manager with some features vim-like.
-  **neovim**: best editor ever, here are some extra features I added to [LazyVim](https://www.lazyvim.org/). 

## Themes

+ **[Gtk theme](https://www.gnome-look.org/p/1267246/)**
+ **[Qt theme](https://aur.archlinux.org/packages/kvantum-theme-nordic-git)**
+ **[Icons](https://github.com/SylEleuth/gruvbox-plus-icon-pack)**

## Installation

1. Clone the repository:

   ```bash
   git clone https://github.com/your-username/dotfiles.git
   cd dotfiles
2. Copy and paste in the correct path, always checking the config.
3. *Anytime just ask for any extra file or info.*


## Screenshots

<img src="images/combine_images.png">