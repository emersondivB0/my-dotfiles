return {
  { "navarasu/onedark.nvim", name = "onedark", lazy = false, opts = { transparent = true } },

  {
    "LazyVim/LazyVim",
    opts = {
      colorscheme = "onedark",
    },
  },
  {
    "rmehri01/onenord.nvim",
    name = "onenord",
    lazy = false,
    opts = { transparent = true },
  },
  {
    require("notify").setup({
      background_colour = "#000000",
    }),
  },
}
